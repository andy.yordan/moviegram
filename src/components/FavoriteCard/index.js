import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { IMSpiderman, IMBond, IMStrong } from '../../assets'
import { Button } from '../../parts'
import { colors } from '../../utils/colors'

const FavoriteCard = ({title, genre, rating, image}) => {
    const ICPoster = () => {
        if(image === 'IMSpiderman'){
            return <Image source={IMSpiderman} style={styles.avatar} />
        }
        if(image === 'IMBond'){
            return <Image source={IMBond} style={styles.avatar} />
        }
        if(image === 'IMStrong'){
            return <Image source={IMStrong} style={styles.avatar} />
        }
        return <Image source={IMSpiderman} style={styles.avatar} />
    }
    return (
        <View style={styles.container}>
            <ICPoster />
            <View style={styles.movieLabel}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.genre}>{genre}</Text>
                <Text style={styles.rating}>{rating}</Text>
            </View>
            <View style={styles.btnDelete}>
                <Button type='icon-only' icon='icon-delete' onPress={() => alert('Available Soon')} />
            </View>            
        </View>
    )
}

export default FavoriteCard

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        paddingVertical: 10,
        borderBottomColor: colors.line,
        borderBottomWidth: 0.5
    },
    movieLabel: {
        flex: 1,
        paddingHorizontal: 5,
        paddingVertical: 10,
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
    btnDelete: {
        justifyContent: 'center'
    },
    avatar: {
        width: 80,
        height: 120
    },
    title: {
        fontSize: 18,
        fontWeight: '700'
    },
    genre: {
        fontSize: 14,
        fontWeight: '400'
    },
    rating: {
        fontSize: 12,
        fontWeight: '300'
    },
})
