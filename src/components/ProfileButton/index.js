import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { ICCard, ICSetting, ICHelp, ICLogout, ICNext } from '../../assets'
import { colors } from '../../utils/colors'

const ProfileButton = ({title, subtitle, icon, onPress}) => {
    const Icon = () => {
        if(icon === 'icon-card'){
            return <ICCard />
        }
        if(icon === 'icon-setting'){
            return <ICSetting />
        }
        if(icon === 'icon-help'){
            return <ICHelp />
        }
        if(icon === 'icon-logout'){
            return <ICLogout />
        }
        return <ICCard />
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Icon />            
            <View style={styles.text}>
                <Text style={styles.title} >{title}</Text>
                <Text>{subtitle}</Text>
            </View>
            <ICNext />
        </TouchableOpacity>
    )
}

export default ProfileButton

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 12,
        borderBottomColor: colors.line,
        borderBottomWidth: 0.5
    },
    text: {
        flex: 1,
        paddingLeft: 16,
        fontSize: 14,
        fontWeight: '300'
    },
    title :{
        fontSize: 18,
        fontWeight: '700'
    }
})
