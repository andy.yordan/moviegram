import React, { useEffect, useState } from 'react'
import { ScrollView, StyleSheet, Image, View, FlatList, Dimensions, TouchableOpacity } from 'react-native'
import { Header } from '../../components'
import { Button } from '../../parts'
import { colors } from '../../utils/colors'
import Axios from 'axios';

const api_key = "15996ef632386ab788354df49c643189";
const BASE_URL = "https://api.themoviedb.org/3";
const getImage = (path) => `https://image.tmdb.org/t/p/w300/${path}`;


let page = "1";
let language = "en-US";

const DEVICE = Dimensions.get('window');

const Search = ({navigation, route}) => {
    const [dataMovie, setDataMovie] = useState([]);
    const [dataGenre, setDataGenre] = useState([]);
    const [type, setType] = useState('upcoming');
    const [genre, setGenre] = useState(null);
    
    const api = Axios.create({ baseURL: BASE_URL });

    const getMovie = (type) => api.get(`movie/${type}`, { params: { api_key, page, language } });
    const getGenreMovie = (id) => {
        let with_genres = id;
        api.get('discover/movie', { params: { api_key, with_genres } });
    }
    const getGenre = api.get("genre/movie/list", { params: { api_key } });

    useEffect(() => {        
        // if(genre === null) {
            getMovie(type).then((res) => {
                setDataMovie(res.data.results);
            });
        // }
        // else {
        //     getGenreMovie(genre).then((res) => {
        //         setDataMovie(res.data.results);
        //     });
        // }
    }, [type, genre]);

    useEffect(() => {                
        getGenre.then((res) => {
            setDataGenre(res.data.genres);
        });
    }, []);

    const handleClickType = (category,type) => {
        if(type === 'type') setType(category)
        else setGenre(category)
    }

    return (
        <View style={styles.container}>
            <Header onPress={() => navigation.goBack()} title='Search' />
            <View style={styles.category}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <View style={styles.categoryButton}>
                        <Button size='small' type='category' title='Upcoming' onPress={() => handleClickType('upcoming', 'type')} />
                    </View>
                    <View style={styles.categoryButton}>
                        <Button size='small' type='category' title='Now Play' onPress={() => handleClickType('now_playing', 'type')} />
                    </View>  
                    <View style={styles.categoryButton}>
                        <Button size='small' type='category' title='Popular' onPress={() => handleClickType('popular', 'type')} />
                    </View>
                    <View style={styles.categoryButton}>
                        <Button size='small' type='category' title='Top Rated' onPress={() => handleClickType('top_rated', 'type')} />
                    </View>                      
                    {dataGenre.map((genre) => (  
                        <View style={styles.categoryButton} key={genre.id}>
                            <Button size='small' type='category' title={genre.name.substring(0,7)} onPress={() => handleClickType(genre.id, 'genre')} />
                        </View>  
                    ))}
                </ScrollView>  
            </View>
            <View style={styles.listWrapper}>
                <FlatList
                    data={dataMovie}
                    renderItem={({item}) => (               
                        <TouchableOpacity style={styles.imageWrapper} onPress={() => navigation.navigate('Detail', movie)}>
                            <Image
                            style={styles.imageThumbnail}
                            source={{uri:getImage(item.poster_path)}}                            
                            />
                        </TouchableOpacity>                   
                    )}
                    numColumns={3}
                    keyExtractor={(item, index) => index}
                />
            </View>             
        </View>
    )
}

export default Search

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.secondary, 
        flex: 1,
        padding: 0,
        margin: 0,
    },
    category: {
        flexDirection: 'row',
        height: 50
    },
    categoryButton: {
        marginLeft: 5,
        width: 85
    },
    listWrapper:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',        
        marginBottom: 10,
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    imageWrapper: {
        width: DEVICE.width * 0.325,
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: 2,
        padding: 0,
    },
    imageThumbnail: {
        flex: 1,
        alignSelf: 'stretch',
        aspectRatio: 0.66,
        width: '100%',
        resizeMode: 'contain',
        borderWidth: 0.5,
        borderColor: 'grey'
    },
})
