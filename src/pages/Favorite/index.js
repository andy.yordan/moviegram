import React from 'react'
import { StyleSheet, View } from 'react-native'
import { FavoriteCard, Header } from '../../components'
import { colors } from '../../utils/colors'

const Favorite = ({navigation}) => {
    return (
        <View style={styles.page}>
            <Header onPress={() => navigation.goBack()} title='My Favorite Movie' />
            <View style={styles.container}>
                <FavoriteCard image='IMSpiderman' title='Spiderman: Far From Home (2020)' genre='Action, Adventure, Drama' rating='2h10m | Rating: 8.5 | Like: 1000' />
                <FavoriteCard image='IMBond' title='No Time To Die (2019)' genre='Action, Adventure, Drama' rating='2h10m | Rating: 8.5 | Like: 1000' />
                <FavoriteCard image='IMStrong' title='12 Strong (2019)' genre='Action, Adventure, Drama' rating='2h10m | Rating: 8.5 | Like: 1000' />
            </View>
        </View>
    )
}

export default Favorite

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary, 
        flex: 1,
        alignItems: 'stretch', 
        padding: 0,
        margin: 0
    },
    container: {
        backgroundColor: colors.primary, 
        flex: 1,
        padding: 10,
        margin: 0,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50
    },
})

