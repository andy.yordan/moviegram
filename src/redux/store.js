import { createStore } from 'redux';

const initialState = {
    loading: false,
    name: 'Yordan Andy',
    address: 'Magelang, Indonesia',
    category: ''
}

const reducer = (state = initialState, action) => {
    if(action.type === 'SET_LOADING') {
        return {
            ...state,
            loading: action.value
        }
    }
    if(action.type === 'SET_CATEGORY') {
        return {
            ...state,
            category: action.value
        }
    }
    return state;
}

const store =createStore(reducer);

export default store;