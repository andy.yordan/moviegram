import ICLogoSplash from './logo-splash.svg'
import ICLogoApp from './logo.svg'
import ICBack from './icon-back.svg'
import ICInstagram from './logo-instagram.png'
import ICTwitter from './logo-twitter.png'
import ICFacebook from './logo-facebook.png'
import ICHome from './icon-home.svg'
import ICSearch from './icon-search.svg'
import ICFavorite from './icon-favorite.svg'
import ICCart from './icon-cart.svg'
import ICProfile from './icon-profile.svg'
import ICCard from './icon-card.svg'
import ICNext from './icon-next.svg'
import ICSetting from './icon-setting.svg'
import ICHelp from './icon-help.svg'
import ICLogout from './icon-logout.svg'
import ICDelete from './icon-delete.svg'

export {
    ICLogoSplash, 
    ICLogoApp, 
    ICBack, 
    ICInstagram, 
    ICTwitter, 
    ICFacebook,
    ICHome,
    ICSearch,
    ICFavorite,
    ICCart,
    ICProfile,
    ICCard,
    ICNext,
    ICSetting,
    ICHelp,
    ICLogout,
    ICDelete,
}