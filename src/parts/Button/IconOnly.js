import React from 'react'
import { TouchableOpacity } from 'react-native'
import { ICBack, ICNext, ICDelete } from '../../assets'

const IconOnly = ({onPress, icon}) => {
    const Icon = () => {
        if(icon === 'icon-back'){
            return <ICBack />
        }
        if(icon === 'icon-next'){
            return <ICNext />
        }
        if(icon === 'icon-delete'){
            return <ICDelete />
        }
        return <ICBack />
    }
    return (
        <TouchableOpacity onPress={onPress}>
            <Icon />
        </TouchableOpacity>
    )
}

export default IconOnly
