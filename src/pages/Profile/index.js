import React, { useState } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { ICFacebook, ICInstagram, ICTwitter, IMProfile } from '../../assets'
import { Header, ProfileButton } from '../../components'
import { Button, Gap } from '../../parts'
import { colors } from '../../utils/colors'
import { useDispatch, useSelector } from 'react-redux'

const Profile = ({navigation}) => {
    const dispatch = useDispatch();
    const stateGlobal = useSelector(state => state)
    const logout = () => {
        dispatch({type: 'SET_LOADING', value: true});
        setTimeout(() => {
            dispatch({type: 'SET_LOADING', value: false});
            navigation.replace('Login')
        }, 2000)
    }

    return (        
        <View style={styles.page}>
            <Header onPress={() => navigation.goBack()} title='User Profile' />
            <View style={styles.container}>
                <View style={styles.profileWrapper}>
                    <View style={styles.avatarWrapper}>
                        <Image source={IMProfile} style={styles.avatar} />
                    </View>
                    <View style={styles.profileLabel}>
                        <Text style={styles.textName}>{stateGlobal.name}</Text>
                        <Text style={styles.textCity}>{stateGlobal.address}</Text>
                        <View style={styles.socmedWrapper}>
                            <Image source={ICInstagram} style={styles.socmed} />
                            <Image source={ICTwitter} style={styles.socmed} />
                            <Image source={ICFacebook} style={styles.socmed} />
                        </View>
                        <Button size='small' title='Edit Profile' onPress={() => alert('Available Soon')} type='secondary' />
                    </View> 
                </View>
                <Gap height={12} />
                <ProfileButton title='Buy Packages & Orders' subtitle='Packages, orders, and invoices' icon='icon-card' onPress={() => alert('Available Soon')} />                               
                <ProfileButton title='Settings' subtitle='Privacy & Security' icon='icon-setting' onPress={() => alert('Available Soon')} />                               
                <ProfileButton title='Help & Support' subtitle='Help center and legal terms' icon='icon-help' onPress={() => alert('Available Soon')} />                               
                <ProfileButton title='Logout' subtitle='Exit application' icon='icon-logout' onPress={logout} />                               
            </View>
        </View>           
    )
}

export default Profile

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary, 
        flex: 1,
        alignItems: 'stretch', 
        padding: 0,
        margin: 0
    },
    container: {
        backgroundColor: colors.primary, 
        flex: 1,
        padding: 20,
        margin: 0,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50
    },
    avatarWrapper: {        
        width: 150,
        height: 150,
        borderWidth: 1,
        borderColor: colors.silver,
        borderRadius: 75,
        alignItems: 'center',
        justifyContent: 'center'
    },  
    avatar: {
        width: 140,
        height: 140
    },
    profileWrapper: {
        flexDirection: 'row',
        paddingBottom: 20,
        borderBottomWidth: 0.5,
        borderBottomColor:  colors.line
    },
    profileLabel: {
        flex: 1,
        paddingLeft: 10,
        alignItems: 'stretch',
        justifyContent: 'space-between'
    },
    textName: {
        fontSize: 24,
        fontWeight: '700',
        alignSelf: 'center'
    },
    textCity: {
        fontSize: 16,
        fontWeight: '300',
        alignSelf: 'center'
    },
    socmedWrapper: {
        flexDirection: 'row',
        alignSelf: 'center',
        height: 40,
        alignItems: 'center'
    },
    socmed: {
        width: 30,
        height: 30,
        marginHorizontal: 8
    },
})
