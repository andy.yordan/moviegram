import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ICBack } from '../../assets'
import { Button, Gap, Input } from '../../parts'
import { colors } from '../../utils/colors'

const Header = ({onPress, title}) => {
    return (
        <View style={styles.container}>
            <Button type='icon-only' icon='icon-back' onPress={onPress} />
            <View style={styles.label}>
                {title === 'Search' ? 
                <Input />
                :
                <>
                <Text style={styles.text}>{title}</Text>      
                <Gap width={20} />      
                </>
                }
            </View>            
            
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    container: {
        padding: 16,
        flexDirection: 'row',
        backgroundColor: colors.secondary,
        alignItems: 'center'
    },
    text: {
        color: colors.white,
        fontSize: 26,
        fontWeight: '600',
    },    
    label: {
        marginLeft: 15,
        flex: 1
    }
})
