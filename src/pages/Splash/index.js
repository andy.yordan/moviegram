import React, { useEffect } from 'react'
import { ImageBackground, StyleSheet, Text, View } from 'react-native'
import { ICLogoSplash, IMBackground } from '../../assets'
import { colors } from '../../utils/colors'

const B = (props) => <Text style={{fontWeight: 'bold'}}>{props.children}</Text>

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Login');
        }, 3000)
    }, [])
    return (
        <ImageBackground source={IMBackground} style={styles.page}>
            <View style={styles.titleContainer}>
                <Text style={styles.title}>Millions of <B>movies</B></Text>
                <Text style={styles.title}>to discover. </Text>
                <Text style={styles.title}><B>Explore</B> now.</Text>                
            </View>
            <ICLogoSplash />            
        </ImageBackground>
    )
}

export default Splash;

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary, 
        flex: 1,
        alignItems: 'center', 
        justifyContent: 'center',
        padding: 40,
    },
    titleContainer: {
        position: 'absolute',
        top: 40,
        left: 40,
    },
    title: {
        color: colors.white,
        fontSize: 32,
        fontWeight: '400'
    },
    titleBold: {
        fontWeight: '600'
    },
})
