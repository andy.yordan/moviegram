# Moviegram

Final Project for React Native Sanbercode

- Aplikasi ini digunakan untuk melihat daftar film dari The Movie Database (TMDB).
- Desain Mockup di Figma link sbb: https://www.figma.com/file/39zbXPss1ggjmShrH0kEdo/Moviegram?node-id=0%3A1
- Rencana yang akan dibangun sesuai mockup figma tsb, tapi berhubung waktu tidak mencukupi ada beberapa fitur yang belum selesai seperti film favorit dan shopping cart
- Rencananya juga akan menggunakan Firebase untuk menyimpan data user dan favorit, tapi berhubung error ketika mendaftar Firebase terpaksa belum jadi
- Daftar API yang digunakan dari TMDB adalah: List Now Playing, Popular, Top Rated, Upcoming, dan Genre Movie
