import IMBackground from './background.png'
import IMProfile from './profile.png'
import IMSpiderman from './im-spiderman.png'
import IMBond from './im-bond.png'
import IMStrong from './im-strong.png'

export {IMBackground, IMProfile, IMSpiderman, IMBond, IMStrong}