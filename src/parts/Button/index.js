import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { colors } from '../../utils/colors'
import IconOnly from './IconOnly'

const Button = ({type, title, onPress, icon, size}) => {
    if(type === 'icon-only') {
        return <IconOnly icon={icon} onPress={onPress} />
    }
    if(type === 'category') {
        return (
            <TouchableOpacity style={styles.container(type, size)} onPress={onPress}>
                <Text style={styles.text(type, size)}>{title}</Text>
            </TouchableOpacity>
        )
    }
    return (        
        <TouchableOpacity style={styles.container(type, size)} onPress={onPress}>
            <Text style={styles.text(type, size)}>{title}</Text>
        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    container: (type, size) => ({ 
        backgroundColor: type === 'secondary' ? colors.tertiary : colors.primary, 
        paddingVertical: size === 'small' ? 8 : 12, 
        borderRadius:10 
    }),
    text: (type, size) => ({ 
        fontSize: size === 'small' ? 16 : 20,  
        fontWeight: '600', 
        textAlign: 'center', 
        color: type === 'secondary' ? colors.primary : colors.black,
    })
})
