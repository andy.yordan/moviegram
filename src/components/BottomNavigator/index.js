import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { TabItem } from '../../parts';
import { colors } from '../../utils/colors';

const BottomNavigator = ({ state, descriptors, navigation }) => {
    const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;
       
        const iconName = ((label) => {
          switch(label){
            case 'Home': return 'home';
            case 'Search': return 'search';
            case 'Favorite': return 'favorite';
            case 'Cart': return 'shopping-cart';
            case 'Profile': return 'person';
            default: return 'home'
          }
        })

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (          
          <TabItem key={index} type={label} iconName={iconName(label)} isFocused={isFocused} onPress={onPress} onLongPress={onLongPress} />         
        );
      })}
    </View>
  );
}

export default BottomNavigator

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: colors.primary,
    justifyContent: 'space-between',
    borderTopWidth: 0.5,
    borderTopColor:  '#444'
    // shadowRadius: 10,
    // shadowOffset: {
    //   width: 0,
    //   height: -10,
    // },
    // shadowOpacity: 1.0,
    // shadowColor: '#444444',
    // elevation: 5,
  },
})
