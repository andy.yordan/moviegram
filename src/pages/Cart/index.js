import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Header } from '../../components'
import { colors } from '../../utils/colors'

const Cart = ({navigation}) => {
    return (
        <View style={styles.page}>
            <Header onPress={() => navigation.goBack()} title='Shopping Cart' />
            <View style={styles.container}>
                <Text>Available soon</Text>
            </View>
        </View>
    )
}

export default Cart

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary, 
        flex: 1,
        alignItems: 'stretch', 
        padding: 0,
        margin: 0
    },
    container: {
        backgroundColor: colors.primary, 
        flex: 1,
        padding: 20,
        margin: 0,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50
    },
})

