import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'


const getImage = (path) => `https://image.tmdb.org/t/p/w300/${path}`;

const CastCard = ({cast}) => {
    return (
        <View style={styles.container}>
            <Image style={styles.castImage} source={{uri:getImage(cast.profile_path)}} />
            <Text>{cast.name.substring(0,12)}</Text>
            <Text>{cast.character.substring(0,12)}</Text>
        </View>
    )
}

export default CastCard

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginRight: 10
    },
    castImage: {
        width: 80,
        height: 80,
        borderWidth: 0.5,
        borderColor: 'grey',
        overflow: 'hidden',
        borderRadius: 40
    },
})
