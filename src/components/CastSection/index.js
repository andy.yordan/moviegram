import React, { useEffect, useState }  from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import Axios from 'axios';
import CastCard from '../CastCard';

const CastSection = ({movieId}) => {
    const [dataCredit, setDataCredit] = useState([]);

    useEffect(() => {
        Axios
            .get(`https://api.themoviedb.org/3/movie/${movieId}/credits?api_key=15996ef632386ab788354df49c643189`)
            .then(res => {
                setDataCredit(res.data.cast);   
            })
            .catch(err => {
                console.log(err)
            })
    }, [])

    return (
        <View style={styles.container}>
            {dataCredit.slice(0,9).map((cast) => (        
                <CastCard key={cast.id} cast={cast} />    
                // <Text>{cast.name}</Text>                   
            ))}
        </View>
    )
}

export default CastSection

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    }
})
