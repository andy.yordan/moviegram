import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { colors } from '../../utils/colors'

const GenreButton = ({title, onPress}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    )
}

export default GenreButton

const styles = StyleSheet.create({
    container: { 
        backgroundColor: colors.primary, 
        paddingVertical:6, 
        borderRadius:20,
        width: 100,
        marginLeft: 10  
    },
    text:{ 
        fontSize:14,  
        fontWeight: '600', 
        textAlign: 'center', 
        color: colors.black,
    }
})
