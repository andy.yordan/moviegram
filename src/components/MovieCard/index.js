import React from 'react'
import { StyleSheet, Text, Image, TouchableOpacity } from 'react-native'
import { colors } from '../../utils/colors'

const MovieCard = ({title, year, image, onPress}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>           
            <Image style={styles.imageThumbnail} source={{uri:image}} />
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.year}>{year}</Text>
        </TouchableOpacity>
    )
}

export default MovieCard

const styles = StyleSheet.create({
    container: {
        marginLeft: 10,
    },
    title: {
        fontSize: 14,
        fontWeight: '600',
        color: colors.white,
        marginTop: 3
    },
    year: {
        fontSize: 12,
        fontWeight: '300',
        color: colors.white
    },
    imageThumbnail: {        
        width: 150,
        height: 200,
        borderWidth: 0.5,
        borderColor: 'grey',
        overflow: 'hidden',
        borderRadius: 10
    },
})
