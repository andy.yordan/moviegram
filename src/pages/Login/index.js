import React from 'react'
import { ImageBackground, StyleSheet, Text, View } from 'react-native'
import { ICLogoApp, IMBackground } from '../../assets'
import { Button, Gap, Input, Link } from '../../parts'
import { colors, showError, useForm } from '../../utils'
import { useDispatch } from 'react-redux'

const Login = ({navigation}) => {
    const [form, setForm] = useForm({email: '', password: ''});
    const dispatch = useDispatch();

    const login = () => {
        console.log('form: ', form);
        dispatch({type: 'SET_LOADING', value: true});
        setTimeout(() => {
            dispatch({type: 'SET_LOADING', value: false});
            // showError('Hello');
            navigation.replace('MainApp')
        }, 3000)
    }

    return (       
        <ImageBackground source={IMBackground} style={styles.page}>
            <ICLogoApp />
            <Text style={styles.title}>Please login to start explore movies </Text>            
            <Input label='Email Address' value={form.email} onChangeText={value => setForm('email', value)} />   
            <Gap height={24} />         
            <Input label='Password' value={form.password} onChangeText={value => setForm('password', value)} secureTextEntry />            
            <Gap height={10} />
            <Link title='Forgot My Password' size={14} />
            <Gap height={40} />                 
            <Button title='Sign In' onPress={login} />
            <Gap height={30} />     
            <Link title='Create New Account' size={18} align='center' />
        </ImageBackground>
    )
}

export default Login;

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary, 
        flex: 1,
        padding: 40,
    },
    title: {
        color: colors.white,
        fontSize: 28,
        fontWeight: '300',
        marginVertical: 60
    },
})
