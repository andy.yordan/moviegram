import React from 'react'
import { StyleSheet, TouchableOpacity, Text } from 'react-native'
import { colors } from '../../utils/colors'
import Icon from 'react-native-vector-icons/MaterialIcons'

const TabItem = ({type, iconName, isFocused, onPress, onLongPress}) => {    
    return (
        <TouchableOpacity onPress={onPress} onLongPress={onLongPress} style={styles.container} >            
            <Icon name={iconName} size={25} color={isFocused ? colors.tertiary : colors.secondary} />
            <Text style={{ color: isFocused ? colors.tertiary : colors.secondary }}>{type}</Text>
        </TouchableOpacity>
    )
}

export default TabItem

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    }
})
