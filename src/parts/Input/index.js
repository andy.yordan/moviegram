import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'
import { colors } from '../../utils/colors'

const Input = ({label, value, onChangeText, secureTextEntry}) => {
    return (
        <View>
            {label && <Text style={styles.label}>{label}</Text> }
            <TextInput style={styles.input} value={value} onChangeText={onChangeText} secureTextEntry={secureTextEntry} />
        </View>
    )
}

export default Input

const styles = StyleSheet.create({
    input: { 
        borderWidth: 1, 
        borderColor: colors.grey, 
        borderRadius: 10, 
        backgroundColor: colors.white, 
        paddingVertical: 6,
        paddingHorizontal: 12
    },
    label: {
        fontSize: 18,
        color: colors.white,
        marginBottom: 6,
        fontWeight: '600'
    }
})
