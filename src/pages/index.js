import Splash from './Splash';
import Login from './Login';
import Home from './Home';
import Search from './Search';
import Favorite from './Favorite';
import Cart from './Cart';
import Profile from './Profile';
import Detail from './Detail';

export {Splash, Login, Home, Search, Favorite, Cart, Profile, Detail};