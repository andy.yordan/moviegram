import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import { colors } from '../../utils/colors'
import Icon from 'react-native-vector-icons/Ionicons'

const PopularCard = ({title, overview, year, popularity, vote_average, image, onPress}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Image style={styles.imageThumbnail} source={{uri:image}} />
            <View style={styles.label}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.overview}>{overview}</Text>
                <Text style={styles.year}>Rel: {year} | Pop: {popularity} | Rate: {vote_average} </Text>
            </View>
            <Icon style={styles.icon} name="chevron-forward" size={50} />
        </TouchableOpacity>
    )
}

export default PopularCard

const styles = StyleSheet.create({
    container: {
        marginLeft: 10,
        marginTop: 12,
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon: {
        color: colors.primary
    },
    label:{
        marginLeft: 14,
        flex: 1,
        justifyContent: 'space-between'
    },
    title: {
        fontSize: 18,
        fontWeight: '600',
        color: colors.white
    },
    overview: {
        fontSize: 12,
        fontWeight: '300',
        color: colors.white,
        flex: 1,
        marginTop: 5
    },
    year: {
        fontSize: 14,
        fontWeight: '300',
        color: colors.white
    },
    imageThumbnail: {        
        width: 75,
        height: 100,
        borderWidth: 0.5,
        borderColor: 'grey',
        overflow: 'hidden',
        borderRadius: 10
    },
})
