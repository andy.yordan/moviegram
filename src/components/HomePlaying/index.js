import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { colors } from '../../utils/colors'

const HomePlaying = ({title, onPress}) => {
    return (
        <View style={styles.container}>
            <View style={styles.headerContainer}>
                <Text style={styles.headerText}>{title}</Text>
                <TouchableOpacity onPress={onPress}>
                    <Text style={styles.viewAll}>View All</Text>
                </TouchableOpacity>                
            </View>
            
        </View>
    )
}

export default HomePlaying

const styles = StyleSheet.create({
    container: {
        paddingTop: 10
    },
    headerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    headerText: {
        color: colors.white,
        fontSize: 22,
        fontWeight: '600',
    },
    viewAll: {
        color: colors.white,
        fontSize: 14,
        fontWeight: '300',
    }

})
