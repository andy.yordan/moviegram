import React, { useEffect, useState } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { ICLogoApp } from '../../assets'
import { HomePlaying, MovieCard } from '../../components'
import PopularCard from '../../components/PopularCard'
import { GenreButton } from '../../parts'
import { colors } from '../../utils/colors'
import Axios from 'axios';

const api_key = "15996ef632386ab788354df49c643189";
const BASE_URL = "https://api.themoviedb.org/3";
const getImage = (path) => `https://image.tmdb.org/t/p/w300/${path}`;

let page = "1";
let language = "en-US";

const Home = ({navigation}) => {
    const [dataNowPlaying, setDataNowPlaying] = useState([]);
    const [dataPopular, setDataPopular] = useState([]);
    const [dataGenre, setDataGenre] = useState([]);

    const api = Axios.create({ baseURL: BASE_URL });

    const getNowPlaying = api.get("movie/now_playing", { params: { api_key, page, language } });
    const getPopular = api.get("movie/popular", { params: { api_key, page, language } });
    const getGenre = api.get("genre/movie/list", { params: { api_key } });

    useEffect(() => {
        getNowPlaying.then((res) => {
            setDataNowPlaying(res.data.results);
        }).catch(err => {console.log(err)});
        getPopular.then((res) => {
            setDataPopular(res.data.results);
        });
        getGenre.then((res) => {
            setDataGenre(res.data.genres);
        });
    }, []);

    return (
        <ScrollView style={styles.container}>
            <ICLogoApp style={styles.title} />
            <HomePlaying title='Now Playing' onPress={() => navigation.navigate('Search')} />
            <View>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <View style={styles.nowPlaying}>
                        {dataNowPlaying.map((movie) => (
                            <MovieCard 
                                key={movie.id} 
                                title={movie.title.substring(0,20)} 
                                year={movie.release_date.substring(0,4)} 
                                image={getImage(movie.poster_path)}
                                onPress={() => navigation.navigate('Detail', movie)}
                            />
                        ))}   
                    </View>  
                </ScrollView>
            </View>
            <View>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <View style={styles.genre}>
                        {dataGenre.map((genre) => (
                            <GenreButton key={genre.id} title={genre.name} onPress={() => navigation.navigate('Search', genre)} />
                        ))}                        
                    </View>  
                </ScrollView>
            </View>
            <HomePlaying title='Popular' onPress={() => navigation.navigate('Search')} />
            <View style={styles.popular}>
                {dataPopular.map((movie) => (
                    <PopularCard 
                        key={movie.id} 
                        title={movie.title.substring(0,25)} 
                        overview={movie.overview.substring(0,120)} 
                        year={movie.release_date.substring(0,4)} 
                        popularity={movie.popularity} 
                        vote_average={movie.vote_average} 
                        image={getImage(movie.poster_path)}
                        onPress={() => navigation.navigate('Detail', movie)}
                    />
                ))} 
            </View>        
        </ScrollView>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.secondary, 
        flex: 1,
        padding: 0,
        margin: 0,
        alignContent: 'flex-start'
    },
    title: {
        width: 250,
        height: 60,
        paddingVertical: 20,
        alignSelf: 'center'
    },
    nowPlaying: {
        flexDirection: 'row',
        paddingTop: 10,        
        height: 250
    },
    genre: {
        flexDirection: 'row',
        paddingTop: 16,
    },
    popular: {
        flex: 1,
        marginBottom: 20
    }
})
