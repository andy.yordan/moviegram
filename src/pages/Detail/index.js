import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ImageBackground, Image, ScrollView, FlatList } from 'react-native'
import { colors } from '../../utils/colors'
import Icon from 'react-native-vector-icons/Ionicons'
import Axios from 'axios';
import { CastSection } from '../../components';

const api_key = "15996ef632386ab788354df49c643189";
const BASE_URL = "https://api.themoviedb.org/3";
const getImage = (path) => `https://image.tmdb.org/t/p/w300/${path}`;

let language = "en-US";

const Detail = ({navigation, route}) => {
    const movie = route.params
    const [dataMovie, setDataMovie] = useState([]);
    // const [dataCredit, setDataCredit] = useState([]);

    const api = Axios.create({ baseURL: BASE_URL });
    const getMovie = (id) => api.get(`movie/${id}`, { params: { api_key, language } });

    useEffect(() => {                
        getMovie(movie.id).then((res) => {
            setDataMovie(res.data);      
        });   
    }, []);

    return (        
        <View style={styles.page}>
            <ImageBackground source={{uri:getImage(dataMovie.backdrop_path)}} style={styles.header} blurRadius={0.5} >
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Icon name='chevron-back-circle' size={40} color={colors.primary} />
                </TouchableOpacity> 
                <TouchableOpacity onPress={() => alert('Share it')}>
                    <Icon name='share-social' size={40} color={colors.primary} />
                </TouchableOpacity> 
            </ImageBackground>
            <View style={styles.container}>
                <View style={styles.headerWrapper}>
                    <View style={styles.posterWrapper}>
                        <Image style={styles.poster} source={{uri:getImage(dataMovie.poster_path)}} />
                    </View>
                    <View style={styles.titleWrapper}>
                        <Text style={styles.title}>{dataMovie.title} </Text>
                        {/* <Text style={styles.content}>{dataMovie.release_date.substring(0,4)} |  {Math.floor(dataMovie.runtime/60)}h{(dataMovie.runtime%60)}m | Rate: {dataMovie.vote_average} </Text>
                        <View style={styles.genreWrapper}>
                            {dataMovie.genres.map((genre) => (                            
                                <Text key={genre.id} style={styles.content}>{genre.name}, </Text>                            
                            ))} 
                        </View> */}
                        <Text style={styles.content}>{dataMovie.tagline} </Text>
                    </View>         
                </View>
                <View style={styles.section}>
                    <Text style={styles.sectionTitle}>Overview</Text>
                    <Text style={styles.content}>{dataMovie.overview}</Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.sectionTitle}>Cast & Crew</Text>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <View style={styles.castWrapper}>
                        <CastSection movieId={dataMovie.id} />
                    </View>                   
                    </ScrollView>
                </View>
            </View>            
        </View>
    )
}

export default Detail

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary, 
        flex: 1,
        padding: 0,
        margin: 0,
        alignContent: 'stretch'
    },
    header: {
        height: 250,
        padding: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.secondary
    },
    container: {
        backgroundColor: colors.primary, 
        flex: 1,
        padding: 20,
        margin: 0,
        marginTop: -50,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50
    },
    headerWrapper: {
        flexDirection: 'row'
    },
    poster: {
        width: 120,
        height: 180,
        borderWidth: 0.5,
        borderColor: 'grey',
        overflow: 'hidden',
        borderRadius: 10,
        marginTop: -80,
    },
    posterWrapper: {        
        shadowColor: "#000",
        shadowOffset: {
            width: 10,
            height: 20,
        },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 5,
    },
    titleWrapper: {
        marginLeft: 10
    },
    title: {
        fontSize: 22,
        fontWeight: '700'
    },
    section: {
        marginTop: 10
    },
    sectionTitle: {
        fontSize: 16,
        fontWeight: '700'
    },
    content: {
        fontSize: 14,
        fontWeight: '300'
    },
    genreWrapper: {
        flexDirection: 'row'
    },
    castWrapper: {
        flex: 1
    }
})
