import firebase from 'firebase'

firebase.initializeApp({
    apiKey: "AIzaSyBFQE9gFicpxsmuYJZYvASyuQ-jcNMPgdI",
    authDomain: "moviegram-ab79d.firebaseapp.com",
    databaseURL: "https://moviegram-ab79d.firebaseio.com",
    projectId: "moviegram-ab79d",
    storageBucket: "moviegram-ab79d.appspot.com",
    messagingSenderId: "42438296446",
    appId: "1:42438296446:web:2a32f61a81eae441b9e92f"
})

const Firebase = firebase;
export default Firebase;