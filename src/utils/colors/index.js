const mainColors = {
    primary: '#FFC312',
    secondary: '#220000',
    tertiary: '#9D0000',
    black: 'black',
    white: 'white',
    grey: '#E9E9E9',
    silver: '#666666',
    line: '#444',
    black1: '#000000',
    black2: 'rgba(0, 0, 0, 0.5)'
}

export const colors = {
    primary: mainColors.primary,
    secondary: mainColors.secondary,
    tertiary: mainColors.tertiary,
    white: mainColors.white,
    black: mainColors.black,
    grey: mainColors.grey,
    silver: mainColors.silver,
    line: mainColors.line,
    text: {
        primary: mainColors.black,
        secondary: mainColors.white
    },
    button: {
        primary: {
            background: mainColors.primary,
            text: mainColors.black
        },
        secondary: {
            background: mainColors.tertiary,
            text: mainColors.primary
        }
    },
    loadingBackground: mainColors.black2
}