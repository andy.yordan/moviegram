import { showMessage } from 'react-native-flash-message'
// import { colors } from '../../utils'

export const showError = (message) => {
    showMessage({
        message: message,
        type: 'default',
        backgroundColor: '#FFC312',
        color: '#220000'
    });
}